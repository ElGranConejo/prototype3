﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject obstaclePf;
    private Vector3 spawnPos = new Vector3(25, 0, 0);
    private float startDelay = 2;
    private PlayerController playerScript;

    // Start is called before the first frame update
    void Start()
    {
        playerScript = GameObject.Find("Player").GetComponent<PlayerController>();
        Invoke("SpawnObstacle", startDelay);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SpawnObstacle()
    {
        if(!playerScript.gameOver)
        {
            Instantiate(obstaclePf, spawnPos, obstaclePf.transform.rotation);
            float repeatRate = Random.Range(1.5f, 2.5f);
            Invoke("SpawnObstacle", repeatRate);
        }
    }
}
